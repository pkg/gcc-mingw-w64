s/@@ARCH@@/any/g
s/@@PACKAGE@@/g++/g
s/@@LANGUAGE@@/C++/g
s/@@DEPENDS32P@@/gcc-mingw-w64-i686-posix (= ${binary:Version}), gcc-mingw-w64-i686-posix-runtime (= ${binary:Version})/g
s/@@DEPENDS32W@@/gcc-mingw-w64-i686-win32 (= ${binary:Version}), gcc-mingw-w64-i686-win32-runtime (= ${binary:Version})/g
s/@@DEPENDS64P@@/gcc-mingw-w64-x86-64-posix (= ${binary:Version}), gcc-mingw-w64-x86-64-posix-runtime (= ${binary:Version})/g
s/@@DEPENDS64W@@/gcc-mingw-w64-x86-64-win32 (= ${binary:Version}), gcc-mingw-w64-x86-64-win32-runtime (= ${binary:Version})/g
s/@@DEPENDSU64@@/gcc-mingw-w64-ucrt64 (= ${binary:Version}), gcc-mingw-w64-ucrt64-runtime (= ${binary:Version})/g
s/@@RECOMMENDS@@//g
s/@@REPLACES32@@/gcc-mingw-w64-bootstrap/g
s/@@REPLACES64@@/gcc-mingw-w64-bootstrap/g
s/@@REPLACESU64@@/gcc-mingw-w64-bootstrap/g
s/@@CONFLICTS32@@/gcc-mingw-w64-bootstrap/g
s/@@CONFLICTS64@@/gcc-mingw-w64-bootstrap/g
s/@@CONFLICTSU64@@/gcc-mingw-w64-bootstrap/g
s/@@BREAKS32@@//g
s/@@BREAKS64@@//g
s/@@BREAKSU64@@//g
